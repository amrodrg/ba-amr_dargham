<h1 align="center">Welcome to MLX-Lab 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1-blue.svg?cacheSeconds=2592000" />
</p>

> An educational User Interface for creating and training deep learning models with tensorflow and keras

## Install

```sh
npm install
```

## Usage

```sh
npm run start
```

## Author

👤 **Amr Dargham**

* Github: [@amrodrg](https://github.com/amrodrg)

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_