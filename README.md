# BA-Amr_Dargham

Die Bachelorarbeit von Amr Dargham.
Das Projekt besteht aus zwei Verzeichnisse: Das erste ist für den Backend-Code und das zweite ist für den Frontend-Code

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://git.imp.fu-berlin.de/amrodrg/ba-amr_dargham.git
git branch -M main
git push -uf origin main
```



## Name
MLX_Lab


## Installation
You can install the requirements for the Backend-Service with: pip -r requirements.txt
To install the Packages for the Frontend-Service enter: npm install


## Contributing
An Education WebInterface for creating and training Machine Learning models


## Authors and acknowledgment
Amr Dargham


